﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace ThreeDigitsSolver
{
    internal class Program
    {
        static IEnumerable<IEnumerable<T>> Subset<T>(IEnumerable<T> source)
        {
            if (!source.Any())
                return Enumerable.Repeat(Enumerable.Empty<T>(), 1);
            var element = source.Take(1);
            var haveNots = Subset(source.Skip(1));
            var haves = haveNots.Select(set => element.Concat(set));
            return haves.Concat(haveNots);
        }

        static void Main(string[] args)
        {
            while (true)
            {
                var numbersStr = Console.ReadLine();
                IEnumerable<int> numbers;
                numbers = numbersStr.Split(',').Select(x => Convert.ToInt32(x));
                foreach (var subset in Subset(numbers))
                {
                    var found = false;
                    var sum1 = subset.Sum();
                    var subset2 = numbers.Except(subset);
                    foreach (var subset3 in Subset(subset2))
                    {
                        var sum3 = subset3.Sum();
                        if (sum1 == sum3)
                        {
                            Console.WriteLine(string.Join(",", subset));
                            Console.WriteLine(string.Join(",", subset3));
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        break;
                }
            }
        }
    }
}
